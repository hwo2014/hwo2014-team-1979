<?php
define('MAX_LINE_LENGTH', 1024 * 1024);
define('LAMBDA',0.0028);

class BasicBot {
	protected $sock, $debug;

	function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
		$this->debug = $debug;
		$this->connect($host, $port, $botkey);
		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	protected function nextPiece($original,$pieces){
		if($original==count($pieces)-1){
			return 0;
		}else{
			return $original++;
		}
	}
	
	protected function ray($pieceIndex,$pieces){
		return $pieces[$pieceIndex]['radius'];		
	}	

	protected function isCurve ($pieceIndex,$pieces){
		return (empty($pieces[$pieceIndex]['length']));
	}
	
	protected function canChangeLane ($pieceIndex,$pieces){
		return (!empty($pieces[$pieceIndex]['switch']));
	}

	public function run() {
		$lap=0;
		$pieces = "";
		$lanes = "";
		$angle = 0;
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
				case 'carPositions':
				
					$myLane = $msg['data'][0]['piecePosition']['lane']['startLaneIndex'];
					
					$lastAngle=$angle;

					// tem que pegar o lane de index=mylane e responder o seu distance from center
					//echo 'lane: '.$myLane,'\n';
					//echo 'lane from center: '.$myLaneFromCenter,'\n';
					
					//precisamos ver qual é o nosso carro para descobrir qual a posição dele
				    $pieceIndex = $msg['data'][0]['piecePosition']['pieceIndex'];	
					
					//Se possível muda a lane para a direita
					//if(!empty($pieces[$pieceIndex]['switch'])){
					//	$this->write_msg('switchLane', "Right");
					//}
					
					if ($this->isCurve($pieceIndex,$pieces)){
						$angle = $msg['data'][0]['angle'];
					}else{
						$angle=0;
					}

					//echo $pieceIndex;				
					$nextPiece = $this->nextPiece($pieceIndex,$pieces);
					$otherNextPiece = $this->nextPiece($nextPiece,$pieces);

					//se as 2 proximas peças forem retas, da-lhe pau
					if(!$this->isCurve($nextPiece, $pieces) && !$this->isCurve($otherNextPiece, $pieces)){
						echo "r";
						$this->write_msg('throttle', 1.0);
					}else {
						echo "c";
						if($this->isCurve($nextPiece,$pieces)){
							$ray= $this->ray($nextPiece,$pieces);	
						}else {
							$ray = $this->ray($otherNextPiece,$pieces);
						}
						
						if ($angle>=40||$angle<=-40){
							if (abs($angle)>abs($lastAngle)){
								echo $angle." ";
								echo "BREAK!!","\n";
								$speed = 0.0;
							}
						} else {
							$speed = sqrt(LAMBDA*$ray);
						}
						//echo $speed,"\n";
						$this->write_msg('throttle', $speed);
					}
					break;
				case 'join':
				case 'yourCar':
				case 'gameInit':
					$pieces = $msg['data']['race']['track']['pieces'];
					$lanes = $msg['data']['race']['track']['lanes'];
					break;
				case 'gameStart':
					echo "Inicio ";
					break;
				case 'crash':
					echo "CRASH!!!","\n";
					break;
				case 'spawn':
					echo "spawn";
					break;
				case 'lapFinished':
					$lap++;
					echo "LAP ".$lap."!!","\n";
					echo "Time ".$msg['data']['lapTime']['millis'],"\n";
					break;
				case 'dnf':
					echo "dnf";
					break;
				case 'finish':
					echo "Terminou a corrida";
					break;
				default:
					$this->write_msg('ping', null);
			}
		}
	}
}
?>
